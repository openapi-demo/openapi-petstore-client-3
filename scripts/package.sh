#!/bin/bash 

SCRIPT_DIR="$( dirname -- "$0"; )"
PROJECT_DIR="$( dirname -- "$SCRIPT_DIR"; )"
ARTIFACTS_DIR="artifacts"

# get info file for a directory
# $1 is the directory
function get_info_file() {
    names="info.yml info.yaml"
    for name in $names ; do
        retval="$1/$name"
        if [ -f $retval ]; then
            return 0
        fi
    done
    return 1
}

# $1 is the source code directory for the client
# $2 is the package name
function package_client() {
    dotnet pack -o nupkgs $1/src/$2/$2.csproj
    dotnet nuget add source "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/nuget/index.json" --name gitlab --username gitlab-ci-token --password $CI_JOB_TOKEN --store-password-in-clear-text
    dotnet nuget push "nupkgs/*.nupkg" --source gitlab
}

#for dir in $PROJECT_DIR/artifacts/* ; do
#    if [ -d $dir ]; then
#        package_client $dir/csharp-netcore
#    fi
#done
package_client artifacts/Petstore/csharp-netcore Petstore.Client
package_client artifacts/TodoApi/csharp-netcore TodoApi.Client
