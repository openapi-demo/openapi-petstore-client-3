#!/bin/bash 

mkdir -p mkdocs/docs/
cp artifacts/Petstore/csharp-netcore/README.md mkdocs/docs/Petstore/
cp artifacts/Petstore/csharp-netcore/docs/* mkdocs/docs/Petstore/

cp artifacts/TodoApi/csharp-netcore/README.md mkdocs/docs/TodoApi/
cp artifacts/TodoApi/csharp-netcore/docs/* mkdocs/docs/TodoApi/

cd mkdocs
mkdocs build
cd ..
mkdir -p public
mv mkdocs/site/* public/
