#!/bin/bash 

SCRIPT_DIR="$( dirname -- "$0"; )"
PROJECT_DIR="$( dirname -- "$SCRIPT_DIR"; )"
ARTIFACTS_DIR="artifacts"


# get info file for a directory
# $1 is the directory
function get_info_file() {
    names="info.yml info.yaml"
    for name in $names ; do
        retval="$1/$name"
        if [ -f $retval ]; then
            return 0
        fi
    done
    return 1
}

# $1 is the output directory of the generator
# $2 is the package name
function post_process_files() {
    post_process_README $1 $2

    for file in $1/docs/*.md ; do
        post_process_doc_file $1 $2 $file
    done
}

# $1 is the output directory of the generator
# $2 is the package name
function post_process_README() {
    # add a newline after "<a ...></a>" line (workaround for using with Docusaurus)
    # sed -i'.original' 's/<\/a>/<\/a>\n/g' $1/README.md
    # rm $1/README.md.original
    
    # add markdown front matter
    echo -e "---\ntitle: $2\n---\n\n$(cat $1/README.md)" > $1/README.md

    # replace links to api and model docs
    sed -i'.original' 's/\*\*](docs\//**](Api\//g' $1/README.md
    sed -i'.original' 's/](docs\//](Model\//g' $1/README.md
    rm $1/README.md.original
}

# $1 is the output directory of the generator
# $2 is the package name
# $3 is the file
function post_process_doc_file() {
    # add a newline after "<a ...></a>" line (workaround for using with Docusaurus)
    # sed -i'.original' 's/<\/a>/<\/a>\n/g' $file
    # rm $file.original
    
    # check the source file is for api or model and add markdown front matter,
    # then move the file to the appropriate directory
    pattern="# $2.([[:alnum:]]+).([[:alnum:]]+)"
    if [[ $( cat $3 ) =~ $pattern ]]; then
        echo -e "---\ntitle: ${BASH_REMATCH[2]}\n---\n\n$(cat $3)" > $3
        new_dir=$1/docs/${BASH_REMATCH[1]}
        mkdir -p $new_dir
        mv $3 $new_dir
    fi
}

# generate clients for an api spec 
# $1 is the directory
function generate_clients() {
    api_name=$( basename $1)
    if ! get_info_file $1; then
        return 1
    fi
    
    info_file=$retval
    echo "### Generating clients for '$api_name'..."
    version=$( $YQ '.version' $info_file )
    if [ $version = "null" ]; then
        echo "Error: No version defined."
        return 1
    fi
    # TODO: check client with version already existing

    spec=$( $YQ '.spec' $info_file )
    if [ $spec = "null" ]; then
        echo "Error: No spec defined."
        return 1
    fi

    spec_file="$1/$spec"
    if [ ! -f $spec_file ]; then
        echo "Error: spec file not found."
        return 1
    fi
    
    n_generators=$( $YQ '.generators | length' $info_file )
    if [ $n_generators -eq 0 ]; then
        echo "Error: No generators defined."
        return 1
    fi
    
    generators=$( $YQ '.generators | keys | .[]' $info_file )
    for generator in $generators ; do
        echo "+ Generating client for '$generator'..."
        outdir=$ARTIFACTS_DIR/$api_name/$generator
        mkdir -p $outdir
        
        config_length=$( $YQ ".generators.$generator | length" $info_file )
        if [ $config_length -gt 0 ]; then
            $YQ ".generators.$generator" $info_file > $outdir/config.yml
            extra_args=" -c $outdir/config.yml"
            package_name=$( $YQ '.packageName' $outdir/config.yml )
        fi

        $OPENAPI_GENERATOR generate -i $spec_file -g $generator -o $outdir \
            --global-property apiTests=false,modelTests=false \
            --skip-validate-spec $extra_args
        post_process_files $outdir $package_name
    done
}


for dir in $PROJECT_DIR/apis/* ; do
    if [ -d $dir ]; then
        generate_clients $dir
    fi
done
